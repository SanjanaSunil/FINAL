from flask import Flask
from flask import render_template
from flask import request, url_for, redirect, session, flash
from datetime import date
from datetime import time
from datetime import datetime
import models as dbHandler

app = Flask(__name__)
app.config['SECRET_KEY'] = 'e5ac358c-f0bf-11e5-9e39-d3b532c10a28'

######################################################################


@app.route('/', methods=['POST', 'GET'])
@app.route('/home', methods=['POST', 'GET'])
def index():
    if 'username' in session:
        username = session['username']
        groups = dbHandler.getGroups(username)
        gd = {}
        for i in groups:
            temp = dbHandler.getGroupMemebers(i[0], i[1])
            gd[i] = temp
            groups=gd
        if request.method == 'POST':
            search = request.form['search']
            if search != username:
                result = dbHandler.searchUser(search)
                if result != "No":
                    return redirect(url_for('anotheruser', result=result))
                return render_template('loggedinindex.html', username=username, result=result, groups=gd)
        return render_template('loggedinindex.html', username=username, groups=gd)
    return render_template('index.html')


@app.route('/register', methods=['POST', 'GET'])
def register():
    if 'username' in session:
        return redirect(url_for('index'))
    if request.method == 'POST':
        username = request.form['username']
        if not dbHandler.uniq(username):
            return render_template('register.html', notUniq="nonunique")

        password = request.form['password']
        email = request.form['email']
        dbHandler.insertUser(username, password, email)
        session['username'] = username
        return redirect(url_for('index'))
    else:
        return render_template('register.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if 'username' in session:
        return redirect(url_for('index'))
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if dbHandler.allowLogin(username, password):
            session['username'] = username
            return redirect(url_for('index'))
        else:
            return render_template('login.html', failedLogin="failed")
    else:
        return render_template('login.html')


@app.route('/logout')
def logout():
    if 'username' in session:
        session.pop('username', None)
    return redirect(url_for('index'))

######################################################################


# 0 represents user not logged in, 1 represents able to send a friend request, 2 represents request sent and 3 is already friends
@app.route('/user/<result>', methods=['POST', 'GET'])
def anotheruser(result):
    if 'username' in session:
        username = session['username']
        if username == result:
            return redirect(url_for('index'))
        if request.method == 'POST' or dbHandler.getStatus(username, result)==2:
            dbHandler.sendRequest(username, result, 2)
            return render_template('anotheruser.html', anotheruser=result, loggedin=2)
        elif dbHandler.getStatus(username, result) == 3:
            return render_template('anotheruser.html', anotheruser=result, loggedin=3)
        elif dbHandler.getStatus(result, username) == 2:
            return render_template('anotheruser.html', anotheruser=result, loggedin=4)
        else:
            return render_template('anotheruser.html', anotheruser=result, loggedin=1)
    return render_template('anotheruser.html', anotheruser=result, loggedin=0)


@app.route('/notifications', methods=['POST', 'GET'])
def notifications():
    if 'username' in session:
        username = session['username']
        friendrequests = dbHandler.getRequests(username)
        if request.method == 'POST':
            for item in friendrequests:
                result = item[0]
                if request.form[result]:
                    if request.form[result] == 'Accept':
                        dbHandler.sendRequest(username, result, 3)
                        dbHandler.sendRequest(result, username, 3)
                    elif request.form[result] == 'Decline':
                        dbHandler.deleteRequest(result, username)
        friendrequests = dbHandler.getRequests(username)
        return render_template('notifications.html', friendrequests=friendrequests)
    return redirect(url_for('login'))


######################################################################


@app.route('/friends')
def getFriends():
    if 'username' not in session:
        return redirect(url_for('login'))
    username = session['username']
    friends = dbHandler.getFriends(username)
    return render_template('friends.html', friends=friends)


#####################################################################


def buildCtrans(temp):
    ctrans = {}
    for i in temp:
        print (i[1:5])
        if (i[1], i[2], i[4], i[3]) in ctrans.keys():
            ctrans[(i[1], i[2], i[4],i[3])] -= i[5]
        elif (i[1], i[2], i[3], i[4]) not in ctrans.keys():
            ctrans[(i[1], i[2], i[3], i[4])] = i[5]
        else:
            ctrans[(i[1], i[2], i[3],i[4])] += i[5]

        if (i[1], i[2], i[3],i[4]) in ctrans.keys() and ctrans[(i[1], i[2], i[3],i[4])] == 0:
            ctrans.pop((i[1], i[2], i[3],i[4]),0)
        if (i[1], i[2], i[4],i[3]) in ctrans.keys() and ctrans[(i[1], i[2], i[4],i[3])] == 0:
            ctrans.pop((i[1], i[2], i[4],i[3]),0)
    return ctrans


names = []
#REMEMBER TO RESET NAMES LIST!!!!!!!!!!!!!!!!!!
@app.route('/new', methods=['POST', 'GET'])
def createGroup():

    global names

    if 'username' not in session:
        return redirect(url_for('login'))

    username = session['username']
    friends = dbHandler.getFriends(username)
    alist = []
    groupname = None
    problem = False

    for friend in friends:
        alist.append(friend[0])

    if request.method == 'POST':
        groupname = request.form['groupname']

        if request.form['add'] == 'Create Group':
            groups = dbHandler.getGroups(username)

            if (groupname, username) not in groups:
                dbHandler.createGroup(groupname, username)
                for name in names:
                    dbHandler.addGroupie(groupname, username, name)
                names = []
                return redirect(url_for('index'))
            else:
                groupname = None
                problem = True

        name = request.form['tags']
        if name in alist:
            if name not in names:
                names.append(name)
    return render_template('createGroup.html', friends=alist, names=names, groupname=groupname, problem=problem)


@app.route('/viewGroup', methods=['POST', 'GET'])
def viewGroup():
    if 'username' not in session:
        return redirect(url_for('login'))

    groupName= request.args.get('groupName')
    admin=request.args.get('admin')
    username = session['username']
    memebers = dbHandler.getGroupMemebers(groupName, admin)

    problem = request.args.get('problem')
    temp = dbHandler.getGroupTrans(groupName, admin)
    ctrans = buildCtrans(temp)

    comments = dbHandler.getComment(groupName, admin)
    print "COMMENTS IS"
    print comments
    vertices = comments[1]
    comments = comments[0]
    print vertices
    print comments

    return render_template('group.html', admin=admin, username=username, groupName=groupName, memebers=memebers, problem=problem, ctrans=ctrans, trans=temp, comments=comments, vertices=vertices)


@app.route('/delGroup/<groupName>', methods=['POST', 'GET'])
def delGroup(groupName):
    if 'username' not in session:
        return redirect(url_for('login'))

    username = session['username']
    dbHandler.delGroup(groupName, username)
    return redirect(url_for('index'))


@app.route('/delGroupie', methods=['POST', 'GET'])
def delGroupie():
    if 'username' not in session:
        return redirect(url_for('login'))

    admin = session['username']
    deletie = request.args.get('deletie')
    groupName = request.args.get('groupName')

    dbHandler.delGroupie(groupName, admin, deletie)

    return redirect(url_for('viewGroup', admin=admin, groupName=groupName))


@app.route('/addGroupie', methods=['POST', 'GET'])
def addGroupie():
    if 'username' not in session:
        return redirect(url_for('login'))

    groupName=request.args.get('groupName')
    rAddr=request.args.get('rAddr')
    username = session['username']

    if request.method == 'POST':

        memebers = dbHandler.getGroupMemebers(groupName, username)
        add = request.form['add']
        if (groupName, username, add) in memebers:
            return redirect(url_for(rAddr, groupName=groupName, admin=username, problem='memeber'))

        friends = dbHandler.getFriends(username)
        for friend in friends:
            if add == friend[0]:
                dbHandler.addGroupie(groupName, username, add)

        return redirect(url_for(rAddr, groupName=groupName, admin=username))


####################################################################


@app.route('/addTrans', methods=['POST', 'GET'])
def addTrans():
    if 'username' not in session:
        return redirect(url_for('login'))

    username = session['username']
    groupName = request.args.get('groupName')
    admin = request.args.get('admin')

    fro=request.form['fro']
    to=request.form['to']
    amt=request.form['amt']

    friends = dbHandler.getGroupMemebers(groupName, admin)
    flag = 0
    for friend in friends:
        if fro == friend[2]:
            flag = flag+1
        if to == friend[2]:
            flag = flag+1

    if fro == username or to == username:
        flag = flag+1
    if fro == to:
        flag = 0

    if flag > 1:
        dbHandler.addGroupTrans(groupName, admin, fro, to, amt)

    return redirect(url_for('viewGroup', admin=admin, groupName=groupName))


@app.route('/delTrans', methods=['POST', 'GET'])
def delTrans():
    if 'username' not in session:
        return redirect(url_for('login'))

    groupName = request.args.get('groupName')
    admin = request.args.get('admin')
    tid = request.args.get('tid')

    dbHandler.delGroupTrans(tid)

    return redirect(url_for('viewGroup', admin=admin, groupName=groupName))

###############################################################


@app.route('/addComment', methods=['POST', 'GET'])
def addComment():
    if 'username' not in session:
        return redirect(url_for('home'))

    admin = request.args.get('admin')
    groupName = request.args.get('groupName')
    par = request.args.get('par')
    comment = request.form['comment']
    username = session['username']

    now = datetime.now()
    commentDate = now.strftime("%a, %d %B, %Y")
    print commentDate

    dbHandler.addComment(groupName, admin, comment, par, username, commentDate)
    return redirect(url_for('viewGroup', groupName=groupName, admin=admin))

##############################################################
if __name__ == '__main__':
    app.run(debug=True)
